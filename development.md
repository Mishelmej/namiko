# Development

Hi there :wave: and thank you for your interest in contributing to this project!

## Prerequisites

Before you are able to contribute to the project, a few tools need to be installed on your maschine:

- [PHP](https://www.php.net/)
- [Composer](https://getcomposer.org/)
- [pre-commit](https://pre-commit.com) (optional)
- [docker](https://docs.docker.com/desktop/) (optional)
- [docker compose](https://docs.docker.com/compose/) (optional)

Check the pages for how to install them.

## Setup

At first we need to install all the dependencies using 

`composer install`.

Composer now takes care of installing all the dependencies into a sub-directory called `vendor`.

Now, there is a small helper called `pre-commit` that allows us to check staged files for a set of rules. In order to activate it, run

`pre-commit install`.

Whenever you make a commit, there will be some checks to enforce the code has a certain shape. If there are issues, the tools would
fix those and you can review them and commit again.

## Running the app

At first we need to put the configuration for the dev setup in place: `cp inc/config.inc.dev.php inc/config.inc.php`.

For running the app locally we provide a ready-to-go `docker compose` configuration you can easily launch. In order to use it, you need to have docker installed and the compose plugin enabled.

At first, make sure to build all required images: `docker compose build`

If all pre-requisites are met, you should be able to run `docker compose up -d` to launch everything thats needed. The app is now served at [localhost:8000](https://localhost:8000).

In order to read the logs, run `docker compose logs -f app`, where the `-f` flag keeps on logging while the app runs.

Now we only need to create all the database tables. This can be done loading `schema.sql` from the repository with the following command:

`sudo docker compose exec -T db mariadb -uroot -pnamiko namiko < schema.sql`

This also create a user you can use: `test@admin.de:test`
