<?php

include "inc/config.inc.php";

function e(string $s): void
{
    echo htmlspecialchars($s);
}

function currency(float $amount): void
{
    global $currency_symbol;
    echo htmlspecialchars($currency_symbol . sprintf("%01.2f", $amount));
}

function datetime(string $datetime): void
{
    $date = new DateTime($datetime);
    echo htmlspecialchars($date->format("d.m.Y H:i"));
}
