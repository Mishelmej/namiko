<?php

//require_once('inc/DBOperations.inc.php');

$currency = '&#8364;'; //currency symbol
$currency_symbol = '€';
$db_host = 'db';
$db_name = 'namiko';
$db_user = 'root';
$db_password = 'namiko';
// We don't want to connect to the database while testing
//$pdo = new PDO("mysql:host=$db_host;charset=utf8;dbname=$db_name", $db_user, $db_password);
//$db = new DBOperations($pdo);

$smtp_host = 'localhost:2525';
$smtp_username = '';
$smtp_password = '';
$smtp_secure = false;

$place = 'Hannover';

$myEmail = 'kontakt@namiko.org';
$myEntity = 'namiko Hannover e.V.';
$myStreet = 'Hahnenstr. 13';
$myRegion = '30167 Hannover';
$myWebsite = 'https://namiko.org/';
$tax_number = '';

$myIBAN = 'DE29430609674127508600';
$myBIC = 'GENODEM1GLS';
$creditorId = 'DE60NAM00002147342';
$tax_rate = 0;

$lastPossibleOrder = '4 days';

$debug = true;
$error_log = __DIR__."/errors.inc.txt";

// reusables
$mail_footer = '
<br><br><br>
<p style="font-style: italic">namiko Hannover e.V.</p>
<p><strong>Bei Rückfragen einfach an kontakt@namiko.org oder bei Basecamp schreiben</strong></p>';

global $currency_symbol;
