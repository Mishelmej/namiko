install:
	composer install
test:
	rm -rf inc/config.inc.php
	cp inc/config.inc.test.php inc/config.inc.php
	./vendor/bin/phpunit tests/ --coverage-text --coverage-filter inc/ --coverage-filter settings/ --coverage-filter controller/ --coverage-cobertura coverage.xml --display-warnings --log-junit test-report.xml

package:
	git diff --quiet || (echo "Working directory is dirty!" && exit 1)
	git describe --tags > inc/version.txt
	rm -rf vendor && mkdir vendor
	rm -rf dist && mkdir dist
	zip -q dist/namiko.zip -r . -x "*/.*" -x ".*" -x "dist**"
