<?php

class OrderController
{
    protected $pdo; // TODO We need to encapsulate all database operations
    private $orderItemsTotal = 0;
    private $lastOrderID = null;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getDeliveredOrders(int $endIndex=0): array
    {
        if ($endIndex === 0) {
            // 0 is the default index, in this case we just want to show the latest
            // entries. Setting all 9s should do the job for a long time
            $endIndex = 999999999999999;
        }
        //Query joins orders and order_items & retrieves productName and price via pid
        $statement = $this->pdo->prepare("
          SELECT orders.*, users.first_name, users.last_name 
          FROM orders
          LEFT JOIN users ON orders.uid = users.uid
          LEFT JOIN order_items ON order_items.oid = orders.oid
          WHERE order_items.delivered = 1
          AND orders.oid <= " . strval($endIndex) . "
          GROUP BY oid
          ORDER BY orders.created_at DESC
          LIMIT 100");
        $statement->execute();
        $allOrders = $statement->fetchAll();
        $this->lastOrderID = end($allOrders)["oid"];
        return $allOrders;
    }

    public function getOrderItems(int $orderId): array
    {
        $statement = $this->pdo->prepare("
          SELECT order_items.pid, products.productName, products.price_KG_L, order_items.quantity, order_items.total 
          FROM order_items 
          LEFT JOIN products ON order_items.pid = products.pid 
          WHERE order_items.oid = ". $orderId);
        $statement->execute();
        $orderItems = $statement->fetchAll();
        $this->orderItemsTotal += array_sum(array_column($orderItems, 'total'));
        return $orderItems;
    }

    public function getOrderItemsTotal(): float
    {
        $val = $this->orderItemsTotal;
        $this->orderItemsTotal = 0;
        return $val;
    }

    public function getLastOrderId(): int
    {
        return $this->lastOrderID;
    }
}
